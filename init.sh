#!/usr/bin/env bash

for site in "$@"; do
	mkdir "$site"
	cat .template/uBlock-rules.txt | sed "s/example.com/$site/" > "$site/uBlock-rules.txt"
	cat .template/uBlock-filters.txt | sed "s/example.com/$site/" > "$site/uBlock-filters.txt"
done

